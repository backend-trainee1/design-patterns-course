public class Singleton {

    private static Singleton singletonInstance = null;

    private Singleton(){

    }

    public static Singleton getInstance() {
        if (singletonInstance == null) {
            singletonInstance = new Singleton();
        }
        return singletonInstance;
    }

    public void connect(){
        System.out.println("I logged in into the DB");
    }

    public void disconnect() {
        System.out.println("I logged out from the DB");
    }

    public static void main(String[] args) {
        Singleton connection = Singleton.getInstance();
        connection.connect();
        connection.disconnect();
    }
}
