package Factory.implementations;

import Factory.IConnection;

public class EmptyDB implements IConnection {


    @Override
    public void connect() {
        System.out.println("No provider specified");
    }

    @Override
    public void disconnect() {
        System.out.println("No provider specified");
    }
}
