package Factory.implementations;

import Factory.IConnection;

public class Oracle implements IConnection {
    private String host, port, user, password;

    public Oracle(){
        this.host = "localhost";
        this.port = "1521";
        this.user = "admin";
        this.password = "123";
    }

    @Override
    public void connect() {
        System.out.println("I logged in into Oracle DB");
    }

    @Override
    public void disconnect() {
        System.out.println("I logged out from Oracle DB");
    }

    @Override
    public String toString(){
        return "ConnectionMySQL [host=" + host + ", port=" + port + ", user=" + user +
                ", password=" + password + "]";
    }
}
