package Factory.implementations;

import Factory.IConnection;

public class MySQL implements IConnection {
    private String host, port, user, password;

    public MySQL(){
        this.host = "localhost";
        this.port = "3306";
        this.user = "root";
        this.password = "123";
    }

    @Override
    public void connect() {
        System.out.println("I logged in into MySQL DB");
    }

    @Override
    public void disconnect() {
        System.out.println("I logged out from MySQL DB");
    }

    @Override
    public String toString(){
        return "ConnectionMySQL [host=" + host + ", port=" + port + ", user=" + user +
                ", password=" + password + "]";
    }
}
