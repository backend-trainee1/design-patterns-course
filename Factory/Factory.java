package Factory;

import Factory.implementations.EmptyDB;
import Factory.implementations.MySQL;
import Factory.implementations.Oracle;

public class Factory {

    public IConnection getConnection(String motor) {
        if (motor.equalsIgnoreCase("MYSQL")) {
            return new MySQL();
        } else if (motor.equalsIgnoreCase("ORACLE")) {
            return new Oracle();
        }

        return new EmptyDB();
    }

    public static void main(String[] args) {
        Factory factory = new Factory();

        IConnection connection = factory.getConnection("mysql");
        connection.connect();
        connection.disconnect();

    }
}
