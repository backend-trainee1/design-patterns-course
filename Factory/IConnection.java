package Factory;

public interface IConnection {
    void connect();
    void disconnect();
}
