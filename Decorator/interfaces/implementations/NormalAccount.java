package Decorator.interfaces.implementations;

import Decorator.Account;
import Decorator.interfaces.BankAccount;

public class NormalAccount implements BankAccount {
    @Override
    public void openAccount(Account account) {
        System.out.println("-------------------------");
        System.out.println("A new account has been open");
        System.out.println("Client: " + account.getClient());
    }
}
