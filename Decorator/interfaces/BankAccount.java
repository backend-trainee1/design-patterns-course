package Decorator.interfaces;

import Decorator.Account;

public interface BankAccount {
    void openAccount(Account account);
}
