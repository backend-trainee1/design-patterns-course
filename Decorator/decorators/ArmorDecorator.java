package Decorator.decorators;

import Decorator.Account;
import Decorator.interfaces.BankAccount;

public class ArmorDecorator extends AccountDecorator{


    public ArmorDecorator(BankAccount decoratedAccount) {
        super(decoratedAccount);
    }

    @Override
    public void openAccount(Account account) {
        decoratedAccount.openAccount(account);

    }

    public void addArmor(Account account) {
        System.out.println("Armor added to customer " + account + " account");
    }
}

