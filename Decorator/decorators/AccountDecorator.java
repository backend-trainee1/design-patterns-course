package Decorator.decorators;

import Decorator.Account;
import Decorator.interfaces.BankAccount;

public class AccountDecorator implements BankAccount {
    protected BankAccount decoratedAccount;

    public AccountDecorator(BankAccount decoratedAccount) {
        this.decoratedAccount = decoratedAccount;
    }

    @Override
    public void openAccount(Account account) {
        this.decoratedAccount.openAccount(account);
    }
}
