package Decorator;

import Decorator.decorators.ArmorDecorator;
import Decorator.interfaces.BankAccount;
import Decorator.interfaces.implementations.SavingsAccount;

public class App {

    public static void main(String[] args) {
        Account client01 = new Account(1, "Daniela");
        Account client02 = new Account(1, "Christian");
        BankAccount account = new SavingsAccount();
        BankAccount armoredAccount = new ArmorDecorator(account);

        account.openAccount(client01);
        armoredAccount.openAccount(client02);
    }
}
