package Proxy;

public class Account {
    private int accountID;
    private String user;
    private double initialBalance;

    public Account(int accountID, String user, double initialBalance) {
        this.accountID = accountID;
        this.user = user;
        this.initialBalance = initialBalance;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public double getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(double initialBalance) {
        this.initialBalance = initialBalance;
    }
}
