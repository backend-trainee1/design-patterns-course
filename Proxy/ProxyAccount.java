package Proxy;

import Proxy.interfaces.IAccount;
import Proxy.interfaces.implementations.BankAccountA;

import java.util.logging.Logger;

public class ProxyAccount implements IAccount {
    private IAccount realAccount;
    private final static Logger LOGGER = Logger.getLogger(ProxyAccount.class.getName());

    public ProxyAccount(IAccount realAccount) {
        this.realAccount = realAccount;
    }

    @Override
    public Account withdrawals(Account account, double amount) {
        LOGGER.info("----Proxy Account - Withdrawals----");
        if (realAccount == null) {
            realAccount = new BankAccountA();
            return realAccount.withdrawals(account, amount);
        }
        else {
            return realAccount.withdrawals(account,amount);
        }
    }

    @Override
    public Account deposit(Account account, double amount) {
        LOGGER.info("----Proxy Account - Deposits----");
        if (realAccount == null) {
            realAccount = new BankAccountA();
            return realAccount.deposit(account, amount);
        }
        else {
            return realAccount.deposit(account,amount);
        }
    }

    @Override
    public void showBalance(Account account) {
        LOGGER.info("----Proxy Account - Show Balance----");
        if (realAccount == null) {
            realAccount = new BankAccountA();
            realAccount.showBalance(account);
        }
        else {
            realAccount.showBalance(account);
        }
    }
}
