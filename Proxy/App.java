package Proxy;

import Proxy.interfaces.IAccount;
import Proxy.interfaces.implementations.BankAccountB;

public class App {
    public static void main(String[] args) {
        Account account = new Account(1, "Daniela", 250.50);

        IAccount proxyAccount = new ProxyAccount(new BankAccountB());
        proxyAccount.showBalance(account);
        account = proxyAccount.withdrawals(account, 80);
        account = proxyAccount.deposit(account, 100);
        proxyAccount.showBalance(account);
    }
}
