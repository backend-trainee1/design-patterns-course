package Proxy.interfaces.implementations;

import Proxy.Account;
import Proxy.interfaces.IAccount;

public class BankAccountB implements IAccount {
    @Override
    public Account withdrawals(Account account, double amount) {
        double currentBalance = account.getInitialBalance() - amount;
        account.setInitialBalance(currentBalance);
        System.out.println("Current balance: $" + account.getInitialBalance());
        return account;
    }

    @Override
    public Account deposit(Account account, double amount) {
        double currentBalance = account.getInitialBalance() + amount + 0.20;
        account.setInitialBalance(currentBalance);
        System.out.println("Current balance: $" + account.getInitialBalance());
        return account;
    }

    @Override
    public void showBalance(Account account) {
        System.out.println("Current balance: $" + account.getInitialBalance());
    }
}
