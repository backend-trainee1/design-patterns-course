package Memento;

public class App {
    public static void main(String[] args) {
        String gameName = "Mario Bros";

        Game game = new Game();
        game.setName(gameName);
        game.setCheckpoint(1);

        Caretaker caretaker = new Caretaker();
        Originator originator = new Originator();

        game = new Game();
        game.setName(gameName);
        game.setCheckpoint(2);
        originator.setState(game);

        game = new Game();
        game.setName(gameName);
        game.setCheckpoint(3);
        originator.setState(game);

        caretaker.addMemento(originator.save()); // index 0

        game = new Game();
        game.setName(gameName);
        game.setCheckpoint(4);
        originator.setState(game);

        caretaker.addMemento(originator.save()); // index 1

        game = new Game();
        game.setName(gameName);
        game.setCheckpoint(5);
        originator.setState(game);

        caretaker.addMemento(originator.save()); // index 2

        originator.setState(game);
        originator.restore(caretaker.getMemento(1));

        game = originator.getState();
        System.out.println(game);
    }

}
