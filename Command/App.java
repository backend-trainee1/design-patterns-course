package Command;

import Command.commands.Account;
import Command.commands.Deposits;
import Command.commands.Invoker;
import Command.commands.Withdrawals;

public class App {
    public static void main(String[] args) {
        Account account = new Account(1, 200);
        Deposits opDeposit = new Deposits(account, 100);
        Withdrawals opWithdrawal = new Withdrawals(account, 50);

        Invoker ivk = new Invoker();
        ivk.getOperation(opDeposit);
        ivk.getOperation(opWithdrawal);

        ivk.operate();
    }
}
