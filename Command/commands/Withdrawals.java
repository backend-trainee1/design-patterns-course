package Command.commands;

public class Withdrawals implements IOperations{
    private Account account;
    private double amount;

    public Withdrawals(Account account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    @Override
    public void execute() {
        this.account.withdrawal(this.amount);
    }
}
