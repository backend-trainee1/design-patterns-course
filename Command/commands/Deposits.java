package Command.commands;

public class Deposits implements IOperations{
    private Account account;
    private double amount;

    public Deposits(Account account, double amount) {
        this.account = account;
        this.amount = amount;
    }

    @Override
    public void execute() {
        this.account.deposit(this.amount);
    }
}
