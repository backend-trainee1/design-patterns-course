package Command.commands;

import java.util.ArrayList;
import java.util.List;

public class Invoker {
    private List<IOperations> operations = new ArrayList<>();

    public void getOperation(IOperations operation) {
        this.operations.add(operation);
    }

    public void operate() {
        this.operations.forEach(x -> x.execute());
        this.operations.clear();
    }
}
