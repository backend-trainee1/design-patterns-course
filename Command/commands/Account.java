package Command.commands;

public class Account {
    private int id;
    private double balance;

    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
    }

    public void withdrawal(double amount) {
        this.balance = this.balance - amount;
        System.out.println("[Withdrawal command] Account: " + this.id +
                " Balance: " + this.balance);
    }

    public void deposit(double amount) {
        this.balance = this.balance + amount;
        System.out.println("[Deposit command] Account: " + this.id +
                " Balance: " + this.balance);
    }
}
