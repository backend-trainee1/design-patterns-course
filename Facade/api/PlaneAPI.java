package Facade.api;

public class PlaneAPI {
    public void searchFlights(String departureDate, String returnDate, String origen, String destination) {
        System.out.println("==============================");
        System.out.println("Flights found for " + destination + " to " + origen);
        System.out.println("Departure Date: " + departureDate + " Return Date " + returnDate);
        System.out.println("==============================");
    }
}
