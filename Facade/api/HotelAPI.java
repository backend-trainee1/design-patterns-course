package Facade.api;

public class HotelAPI {
    public void searchHotels(String entryDate, String exitDate, String origen, String destination) {
        System.out.println("==============================");
        System.out.println("Found Hotels");
        System.out.println("Entry: " + entryDate + " Exit: " + exitDate);
        System.out.println("Hotel A");
        System.out.println("Hotel B");
        System.out.println("Hotel C");
        System.out.println("==============================");
    }
}
