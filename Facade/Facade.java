package Facade;

import Facade.api.HotelAPI;
import Facade.api.PlaneAPI;

public class Facade {
    private PlaneAPI planeAPI;
    private HotelAPI hotelAPI;

    public Facade() {
        this.planeAPI = new PlaneAPI();
        this.hotelAPI = new HotelAPI();
    }

    public void search(String departureDate, String returnDate, String origen, String destination) {
        planeAPI.searchFlights(departureDate, returnDate, origen, destination);
        hotelAPI.searchHotels(departureDate, returnDate, origen, destination);
    }
}
