package Facade;

public class App {
    public static void main(String[] args) {
        Facade client01 = new Facade();
        client01.search("02/07/2018", "08/07/2018", "Lima", "Cancun");

        Facade client02 = new Facade();
        client02.search("02/07/2018", "08/07/2018", "Lima", "Quito");
    }
}
