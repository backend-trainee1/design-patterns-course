package Strategy;

public abstract class AdvancedAnalysis implements IStrategy {

    public void analyse() {
        start();
        analyseMemory();
        analyseKeyLoggers();
        analyseRootKits();
        unzip();
        stop();
    }

    abstract void start();
    abstract void analyseMemory();
    abstract void analyseKeyLoggers();
    abstract void analyseRootKits();
    abstract void unzip();
    abstract void stop();
}
