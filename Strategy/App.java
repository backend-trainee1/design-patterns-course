package Strategy;

public class App {
    public static void main(String[] args) {
        Context context = new Context(new AdvancedAntivirus());
        context.execute();
    }
}
