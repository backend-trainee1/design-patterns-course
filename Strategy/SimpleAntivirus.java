package Strategy;

public class SimpleAntivirus extends SimpleAnalysis{

    @Override
    void start() {
        System.out.println("Simple Antivirus -> The analyse has started");
    }

    @Override
    void skipZip() {
        try {
            System.out.println("Analysing...");
            Thread.sleep(2500);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    void stop() {
        System.out.println("Simple Antivirus -> The analyse has stopped");
    }
}
