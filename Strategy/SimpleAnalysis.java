package Strategy;

public abstract class SimpleAnalysis implements IStrategy{

    public void analyse() {
        start();
        skipZip();
        stop();
    }

    abstract void start();
    abstract void skipZip();
    abstract void stop();

}
