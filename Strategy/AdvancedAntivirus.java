package Strategy;

public class AdvancedAntivirus extends AdvancedAnalysis{
    @Override
    void start() {
        System.out.println("Advanced Antivirus -> The analyse has started");
    }

    @Override
    void analyseMemory() {
        try {
            System.out.println("Analyzing RAM...");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    void analyseKeyLoggers() {
        try {
            System.out.println("Searching for Keyloggers...");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    void analyseRootKits() {
        try {
            System.out.println("Searching for RootKits...");
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    void unzip() {
        try {
            System.out.println("Analyzing zip files...");
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    void stop() {
        System.out.println("Simple Antivirus -> The analyse has stopped");
    }
}
