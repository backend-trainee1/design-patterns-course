package AbstractFactory;

import AbstractFactory.Interfaces.AbstractFactory;
import AbstractFactory.Interfaces.IDBConnection;
import AbstractFactory.Interfaces.IRESTConnection;
import AbstractFactory.Interfaces.implementations.EmptyDB;
import AbstractFactory.Interfaces.implementations.MySQL;
import AbstractFactory.Interfaces.implementations.Oracle;

public class FactoryDBConnection implements AbstractFactory {

    @Override
    public IDBConnection getDB(String motor) {
        if(motor.equalsIgnoreCase("MYSQL")) {
            return new MySQL();
        }
        else if (motor.equalsIgnoreCase("ORACLE")) {
            return new Oracle();
        }

        return new EmptyDB();
    }

    @Override
    public IRESTConnection getREST(String area) {
        return null;
    }
}
