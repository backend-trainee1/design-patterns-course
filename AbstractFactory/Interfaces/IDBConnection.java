package AbstractFactory.Interfaces;

public interface IDBConnection {
    void connect();
    void disconnect();
}
