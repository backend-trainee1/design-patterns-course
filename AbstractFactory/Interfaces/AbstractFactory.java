package AbstractFactory.Interfaces;

public interface AbstractFactory {
    IDBConnection getDB(String motor);
    IRESTConnection getREST(String area);
}
