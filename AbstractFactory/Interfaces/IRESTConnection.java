package AbstractFactory.Interfaces;

public interface IRESTConnection {
    void readURL(String url);
}
