package AbstractFactory.Interfaces.implementations;

import AbstractFactory.Interfaces.IRESTConnection;

public class RESTNoAreaConnection implements IRESTConnection {
    @Override
    public void readURL(String url) {
        System.out.println("No chosen area");
    }
}
