package AbstractFactory.Interfaces.implementations;

import AbstractFactory.Interfaces.IDBConnection;

public class EmptyDB implements IDBConnection {

    @Override
    public void connect() {
        System.out.println("No provider specified");
    }

    @Override
    public void disconnect() {
        System.out.println("No provider specified");
    }
}
