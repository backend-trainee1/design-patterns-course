package AbstractFactory.Interfaces.implementations;

import AbstractFactory.Interfaces.IDBConnection;

public class Oracle implements IDBConnection {
    private String host, port, user, password;

    public Oracle(){
        this.host = "localhost";
        this.port = "3306";
        this.user = "root";
        this.password = "123";
    }

    @Override
    public void connect() {
        System.out.println("I logged in into Oracle DB");
    }

    @Override
    public void disconnect() {
        System.out.println("I logged out from Oracle DB");
    }

    @Override
    public String toString(){
        return "ConnectionMySQL [host=" + host + ", port=" + port + ", user=" + user +
                ", password=" + password + "]";
    }
}
