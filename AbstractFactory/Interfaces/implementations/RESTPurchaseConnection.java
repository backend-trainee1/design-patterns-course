package AbstractFactory.Interfaces.implementations;

import AbstractFactory.Interfaces.IRESTConnection;

public class RESTPurchaseConnection implements IRESTConnection {
    @Override
    public void readURL(String url) {
        System.out.printf("Connecting into %s", url);
    }
}
