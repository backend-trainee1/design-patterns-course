package AbstractFactory;

import AbstractFactory.Interfaces.AbstractFactory;
import AbstractFactory.Interfaces.IDBConnection;
import AbstractFactory.Interfaces.IRESTConnection;

public class App {
    public static void main(String[] args) {
        AbstractFactory factoryDB = FactoryProducer.getFactory("DB");
        IDBConnection dbConnection = factoryDB.getDB("MySql");
        dbConnection.connect();

        AbstractFactory factoryREST = FactoryProducer.getFactory("Rest");
        IRESTConnection restConnection = factoryREST.getREST("Purchase");
        restConnection.readURL("https://www.youtube.com/subscription_center?add_user=mitocode");
    }
}
