package AbstractFactory;

import AbstractFactory.Interfaces.AbstractFactory;

public class FactoryProducer {
    public static AbstractFactory getFactory(String factoryType) {
        if (factoryType.equalsIgnoreCase("DB")) {
            return new FactoryDBConnection();
        } else if (factoryType.equalsIgnoreCase("REST")) {
            return new FactoryRESTConnection();
        }

        return null;
    }
}
