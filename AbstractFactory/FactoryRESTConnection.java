package AbstractFactory;

import AbstractFactory.Interfaces.AbstractFactory;
import AbstractFactory.Interfaces.IDBConnection;
import AbstractFactory.Interfaces.IRESTConnection;
import AbstractFactory.Interfaces.implementations.RESTNoAreaConnection;
import AbstractFactory.Interfaces.implementations.RESTPurchaseConnection;
import AbstractFactory.Interfaces.implementations.RESTSalesConnection;

public class FactoryRESTConnection implements AbstractFactory {
    @Override
    public IDBConnection getDB(String motor) {
        return null;
    }

    @Override
    public IRESTConnection getREST(String area) {
        if (area.equalsIgnoreCase("SALES")) {
            return new RESTSalesConnection();
        } else if (area.equalsIgnoreCase("PURCHASE")) {
            return new RESTPurchaseConnection();
        }

        return new RESTNoAreaConnection();
    }
}
