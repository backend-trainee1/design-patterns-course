package Prototype;

public interface IAccount {
    IAccount clone();
}
