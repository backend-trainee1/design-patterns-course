package Prototype;

public class App {

    public static void main(String[] args) {
        Prototype savingsAccount01 = new Prototype();
        savingsAccount01.setAmount(200);

        Prototype savingsAccount02 = new Prototype();
        Prototype cloneAccount = (Prototype) savingsAccount01.clone();

        System.out.println(savingsAccount01);
        System.out.println(savingsAccount02);
        System.out.println(cloneAccount);


    }
}
