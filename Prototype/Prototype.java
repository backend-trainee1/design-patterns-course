package Prototype;

public class Prototype implements IAccount{

    private String type;
    private double amount;

    @Override
    public Prototype clone() {
        Prototype account = null;

        account = (Prototype) clone();

        return account;
    }

    @Override
    public String toString() {
        return "Account [type= " + type + " amount= " + amount + "]";
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

}
