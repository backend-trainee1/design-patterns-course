package Observer;

public class App {
    public static void main(String[] args) {
        Subject subject = new Subject();

        new SolObserver(subject);
        new ARGObserverPesos(subject);
        new MXNObserverPesos(subject);

        System.out.println("10 dollars change");
        subject.setState(10);
        System.out.println("150 dollars change");
        subject.setState(150);
    }
}
