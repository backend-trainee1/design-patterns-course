package Observer;

public class ARGObserverPesos extends Observer{

    private double changeValue = 29.86;

    public ARGObserverPesos(Subject subject) {
        this.subject = subject;
        this.subject.add(this);
    }

    @Override
    public void update() {
        System.out.println("ARG: " + (subject.getState() * changeValue));
    }
}
