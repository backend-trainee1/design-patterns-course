package Observer;

public class MXNObserverPesos extends Observer{
    private double changeValue = 19.07;

    public MXNObserverPesos(Subject subject) {
        this.subject = subject;
        this.subject.add(this);
    }

    @Override
    public void update() {
        System.out.println("MXN: " + (subject.getState() * changeValue));
    }
}
