package Observer;

public class SolObserver extends Observer{
    private double changeValue = 3.25;

    public SolObserver(Subject subject) {
        this.subject = subject;
        this.subject.add(this);
    }

    @Override
    public void update() {
        System.out.println("PEN: " + (subject.getState() * changeValue));
    }
}
